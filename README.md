DUNE-VEM-PAPER
==============

[DUNE-VEM][20] is a [Distributed and Unified Numerics Environment][1]
module which provides implementation of a range of virtual element
spaces. It is based on the interfaces defined in [DUNE-FEM][0].
In addition to the C++ implementation an extensive Python interface
is provided.

A detailed description of the VEM implementation is given in our paper
[A framework for implementing general virtual element spaces][21].
Please cite this paper, if you find this module useful for your research.

The numerical examples provided in the paper are included in this repository.
Many of the scripts are also included in the dune-vem PyPi package and git
repository, where they will be kept up to date with the development of the DUNE code.
The results from the paper are based on the 2.9.0 version and the scripts
to reproduce the results from the paper are made available here, together
with a configuration and run script.

**Note**: this package requires *Python3* and a recent C++ compiler (e.g. g++ or clang).

If Dune with its Python bindings have not yet been installed then the
``configure`` script will setup a new virtual environment and install
``dune-vem`` and other required modules. After cloning this repository
simply run
```
./configure
```

The new virtual environment will be located in ``dune-env`` and should be
activated before running any of the scripts by executing
```
source dune-env/bin/activate
```

This repository contains the following Python scripts corresponding to the
different subsection of Section 7 in the paper:

<table class="tg">
<thead>
  <tr>
    <th class="tg-jlhb">Script</th>
    <th class="tg-jlhb">Section</th>
    <th class="tg-jlhb">Description</th>
    <th class="tg-jlhb">Parameters</th>
    <th class="tg-jlhb">Output</th>
  </tr>
</thead>
<tbody>
  <tr>
    <td class="tg-0lax">laplace.py</td>
    <td class="tg-0lax">7.1</td>
    <td class="tg-0lax">Laplace problem in primal and<br>dual form on a Voronoi grid</td>
    <td class="tg-0lax">None</td>
    <td class="tg-0lax">pngs of solution and grids<br>Note: the seeds for the voronoi cells are random<br>causing results to differ between runs</td>
  </tr>
  <tr>
    <td class="tg-0lax">mixedSolver.py</td>
    <td class="tg-0lax">7.1<br></td>
    <td class="tg-0lax">utility: solver for mixed problem</td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
  </tr>
  <tr>
    <td class="tg-0lax">testStabilization.py</td>
    <td class="tg-0lax">7.2</td>
    <td class="tg-0lax">Stabilized and non-stabilized<br>schemes for Laplace and<br>Bilaplace problem<br></td>
    <td class="tg-0lax">-L max-level &gt; 0<br>- l polynomial order &gt;= 1<br>-p problem = [laplace|biharmonic]<br>-s use stabilization = [-1|0|1]<br>
    &emsp;1: standard space<br>
    &emsp;0: no stabilization and<br>&emsp;&emsp;standard projection<br>
    &emsp;-1: no stabilization and<br>&emsp;&emsp;projection of gradient, hessian<br>&emsp;&emsp;into P_l, P_{l-1}, respectively.</td>
    <td class="tg-0lax">errors and convergence rates</td>
  </tr>
  <tr>
    <td class="tg-0lax">cylinder.py</td>
    <td class="tg-0lax">7.3<br></td>
    <td class="tg-0lax">Compare different discretizations<br>for incompressible flow problems<br>around a cylinder</td>
    <td class="tg-0lax">-l polynomial order &gt;= 2<br>-s spaces = [vem|th]<br>-g grid = [0 (coarse)|1 (fine)]</td>
    <td class="tg-0lax">time series vtk files</td>
  </tr>
  <tr>
    <td class="tg-0lax">uzawa.py</td>
    <td class="tg-0lax">7.3<br></td>
    <td class="tg-0lax">utility: solver for Stokes problem</td>
    <td class="tg-0lax"></td>
    <td class="tg-0lax"></td>
  </tr>
  <tr>
    <td class="tg-0lax">willmore.py</td>
    <td class="tg-0lax">7.4</td>
    <td class="tg-0lax">Willmore flow of a graph</td>
    <td class="tg-0lax">None</td>
    <td class="tg-0lax">time series vtk files</td>
  </tr>
</tbody>
</table>

A script is provided to produce results for a specific subsection. Run
```
./run subsection
```
where ``subsection`` is one of ``7.1``, ``7.2``, ``7.3``, or ``7.4``.
In some cases not all results are produced to reduce computational cost.
This will be mentioned in the output of the script and the changes to ``run``
needed to produced the remaining results are clearly detailed in the script.

**Note**: the *Willmore flow* example from Section 7.4 requires the
*Umfpack* solver which is part of [SuiteSparse][15] which needs to be
installed first before configuring the virtual environment to run this example.

General Tutorial
----------------

The more general [DUNE-FEM tutorial][18] includes a number of further examples showcasing the DUNE-VEM module
and provides an overview of DUNE.

License
-------

The DUNE-VEM library, headers and test programs are free open-source software,
licensed under version 2 or later of the GNU General Public License.

See the file [LICENSE][7] for full copying permissions.


References
----------

A detailed description of DUNE-VEM and the uderlying DUNE-FEM module can be found in

* A. Dedner, A. Hodson. A framework for implementing general virtual * element space.
  https://arxiv.org/abs/2208.08978

* A. Dedner, R. Klöfkorn, M. Nolte, and M. Ohlberger. A Generic Interface for Parallel and Adaptive Scientific Computing:
  Abstraction Principles and the DUNE-FEM Module.
  Computing, 90(3-4):165--196, 2010. http://dx.doi.org/10.1007/s00607-010-0110-3

* A. Dedner, R. Klöfkorn, and M. Nolte. Python Bindings for the DUNE-FEM module.
  Zenodoo, 2020 http://dx.doi.org/10.5281/zenodo.3706994

* P. Bastian, M. Blatt, A. Dedner, N.-A. Dreier, C. Engwer, R. Fritze, C. Gräser, C. Grüninger, D. Kempf, R. Klöfkorn, M. Ohlberger, O. Sander.
  The DUNE framework: Basic concepts and recent developments.
  Computers & Mathematics with Applications 81, 2021 https://www.sciencedirect.com/science/article/pii/S089812212030256X?via%3Dihub


 [0]: https://www.dune-project.org/modules/dune-fem/
 [1]: https://www.dune-project.org
 [2]: https://www.dune-project.org/doc/installation/
 [3]: http://www.mcs.anl.gov/petsc/
 [4]: http://eigen.tuxfamily.org
 [5]: http://lists.dune-project.org/mailman/listinfo/dune-fem
 [6]: http://gitlab.dune-project.org/dune-fem/dune-fem/issues
 [7]: LICENSE.md
 [8]: http://gitlab.dune-project.org/extensions/dune-alugrid
 [9]: http://gitlab.dune-project.org/extensions/dune-spgrid
 [10]: http://gitlab.dune-project.org/core/dune-common
 [11]: http://gitlab.dune-project.org/core/dune-geometry
 [12]: http://gitlab.dune-project.org/core/dune-grid
 [13]: http://gitlab.dune-project.org/core/dune-istl
 [14]: http://gitlab.dune-project.org/core/dune-localfunctions
 [15]: http://faculty.cse.tamu.edu/davis/suitesparse.html
 [16]: http://www.fz-juelich.de/jsc/sionlib
 [17]: http://icl.cs.utk.edu/papi/software/index.html
 [18]: https://dune-project.org/sphinx/content/sphinx/dune-fem/
 [19]: https://gitlab.dune-project.org/dune-fem/dune-fem/-/pipelines/
 [20]: https://www.dune-project.org/modules/dune-vem/
 [21]: https://arxiv.org/abs/2208.08978
